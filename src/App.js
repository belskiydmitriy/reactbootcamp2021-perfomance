import React, { memo, useEffect, useState } from "react";
import "./App.css";

const useFetch = (url) => {
  const [status, setStatus] = useState({
    data: undefined,
    error: undefined
  });

  function fetchNow(url) {
    console.count("render useFetch");
    fetch(url)
      .then((res) => res.json())
      .then((res) => {
        setStatus({ data: res.data });
      })
      .catch((error) => {
        setStatus({ error });
      });
  }

  useEffect(() => {
    if (url) {
      fetchNow(url);
    }
  }, [url]);

  return { ...status };
}

let Button = memo((props) => {
  console.count("render button");
  return <button {...props} style={{ backgroundColor: "lightgray" }} >Change sort direction</button>;
}, (prevProps, nextProps) => prevProps.children === nextProps.children)

let ListItem = memo(({ children }) => {
  console.count("render list item");
  return (
    <li>
      {children}
      <label style={{ fontSize: "smaller" }}>
        <input type="checkbox" />
        Add to cart
      </label>
    </li>
  );
})

function App() {
  const [searchString, setSearchString] = useState("");
  const [isSortingDesc, setSortingDesc] = useState(false);
  const [products, setProducts] = useState([]);
  const { data, error } = useFetch('https://reqres.in/api/products')

  useEffect(() => {
    console.count("render filter&sort");
    if (data) {
      setProducts(
        data
          .filter((item) => item.name.includes(searchString))
          .sort((a, z) =>
            isSortingDesc
              ? z.name.localeCompare(a.name)
              : a.name.localeCompare(z.name)
          )
      )
    }
  }, [data, searchString, isSortingDesc]);

  console.count("render app");

  if (error) {
    console.log("error", error);
    return null;
  }

  return (
    <div className="App">
      <input
        type="search"
        value={searchString}
        onChange={(e) => {
          setSearchString(e.target.value)
        }}
      />
      <Button onClick={() => setSortingDesc((value) => !value)}>
        Change sort direction
      </Button>
      <ul>
        {products.map((product) => {
          return <ListItem key={product.id}>{product.name}</ListItem>;
        })}
      </ul>
    </div>
  );
}

export default App;
